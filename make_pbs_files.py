"""Create PBS files and a multirunner for htc files in a directory.
Can either be all htc files, or those that don't have a result file.
"""
import os
from wetb.hawc2 import HTCFile
from wetb.hawc2.hawc2_pbs_file import wine_cmd
from wetb.utils.cluster_tools.pbsfile import PBSMultiRunner
from _utils import setup_dir

if __name__ == '__main__':
    # script inputs
    htc_dir = '../htc_steady/'  # top folder w/htc files (end w/slash!)
    regen = False  # resimulate even if results exist?
    h2_cwd = '../'  # hawc2's working directory (end w/slash!)
    # cluster settings
    queue = 'xpresq'  # queue to submit job to
    ind_walltime = 30  # walltime of each individual job (minutes)
    all_walltime = 30  # walltime of all simulations (minutes)
    nnodes = 2  # number of nodes to use for job
    # hawc2 configurations
    version = "v12.8.0.0"
    platform = "win32"
    hawc2_path="/mnt/aiolos/groups/hawc2sim/HAWC2/%s/%s/" % (version, platform)
    hawc2_cmd = wine_cmd(platform='win32', hawc2='hawc2mb.exe', cluster='jess')
    # intermediate variables
    workdir = os.path.abspath('..')
    print('Generating pbs files...')
    setup_dir('../pbs_in/')  # check pbs directory exists
    pbs_lst = []
    for root, dirs, files in os.walk(htc_dir):
        for file in files:
            if '_master' in file:
                continue
            if file.endswith('.htc'):
                htc = HTCFile(os.path.join(root, file))
                res_path = h2_cwd + str(htc.output.filename.values[0]) + '.dat'
                if (os.path.exists(res_path) & (not regen)):  # if res exists & we're not regenerating
                    if os.path.getsize(res_path):  # also if res is nonzero size
                        continue  # then don't simulate the file
                pbs = htc.pbs_file(hawc2_path, hawc2_cmd,
                               queue=queue, # workq, windq, xpresq
                               walltime=ind_walltime*60, # individual wall time in seconds
                               input_files=None, # If none, required files are autodetected from htc file
                               output_files=None, # If none, output files are autodetected from htc file
                               copy_turb=(True, True) # copy turbulence files (to, from) simulation
                              )
                pbs.workdir = workdir  # update work directory (default is wrong)
                pbs.save()
                os.chmod(pbs.workdir + '/' + pbs.filename, 0o774)  # double-check permissions are ok
                pbs_lst.append(pbs)
    print(f'{len(pbs_lst)} files to simulate.')
    # make multirunner
    pbs_all = PBSMultiRunner(workdir=workdir,
                        queue=queue, # alternatives workq, windq, xpresq
                        walltime=all_walltime*60,   # expected total simulation time in seconds
                        nodes=nnodes,       # Number of nodes
                        ppn=20,         # number of processors of each node (normally 20)
                        pbsfiles=pbs_lst  # If None, the multirunner searches for *.in files
                        )
    pbs_all.save()
    print('Multirunner file: ', os.path.join(workdir, pbs_all.filename))