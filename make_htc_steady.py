"""Make htc files for a series of steady winds
"""
import os
import numpy as np
from wetb.hawc2 import HTCFile
from wetb.hawc2.hawc2_pbs_file import wine_cmd
from wetb.utils.cluster_tools.pbsfile import PBSMultiRunner
from _utils import setup_dir

if __name__ == '__main__':
    # script inputs
    wsps = np.arange(3, 26, 0.5)  # wind speeds to simulate
    htc_dir = '../htc_steady/'  # directory to store htc files (end w/slash!)
    master_dir = f'../htc_master/'  # location of htc master file (normal turb. simulation)
    tstart, tstop = 400, 500  # start and stop times for saving output
    clean_htc_dir = True  # delete existing files in htc_dir?
    # intermediate values
    model_key = os.path.basename(os.path.abspath('..'))  # key for model directory, e.g. A0001
    template_path = [os.path.join(master_dir, f) for f in
                     os.listdir(master_dir) if f.endswith('.htc')][0]
    setup_dir(htc_dir, clean_dir=clean_htc_dir)
    # make htc files for each wind speeds
    for wsp in wsps:
        basename = f'{model_key}_wsp{wsp}'  # e.g., A0001_wsp6
        htc_path = f'./{htc_dir}{basename}.htc'
        # update values
        htc = HTCFile(template_path)
        # simulation
        htc.simulation.time_stop = tstop
        htc.simulation.logfile = f'./log/{basename}.log'
        # wind
        htc.wind.wsp = wsp
        htc.wind.tint = 0
        htc.wind.shear_format = [1, wsp]
        htc.wind.turb_format = 0
        del htc.wind.mann
        # output
        htc.output.time = [tstart, tstop]
        htc.output.filename = f'./res/{basename}'
        # save file
        htc.save(htc_path)
