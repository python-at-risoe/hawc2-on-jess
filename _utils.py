"""Make htc files for running steady-state
"""
import os
import shutil

def setup_dir(dirpath, clean_dir=False):
    """Setup a directory by either making it, cleaning it, or skipping"""
    if os.path.exists(dirpath) and clean_dir:
        shutil.rmtree(dirpath, ignore_errors=True)
    if not os.path.exists(dirpath):
        os.mkdir(dirpath)
